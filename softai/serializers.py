from rest_framework.serializers import ModelSerializer 
from clients.models import Clients
from employees.models import Employees

class ClientsSerializer(ModelSerializer):
 
    class Meta:
        model = Clients
        fields = ['id', 'name']
class EmployeesSerializer(ModelSerializer):
 
    class Meta:
        model = Employees
        fields = ['id', 'name']