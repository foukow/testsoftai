from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet
from employees.models import Employees
from serializers import EmployeesSerializer
from rest_framework.permissions import IsAuthenticated
from employees.permissions import HasCredentials
 
class EmployeesAPIView( APIView):
 
    def get(self, *args, **kwargs):
        categories = Employees.objects.all()
        serializer = EmployeesSerializer(categories, many=True)
        return Response(serializer.data)

class EmployeesViewset(ReadOnlyModelViewSet):
 
    serializer_class = EmployeesSerializer
    permission_classes=[IsAuthenticated, HasCredentials]
    def get_queryset(self):
        return Employees.objects.all()