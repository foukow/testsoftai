from rest_framework.permissions import BasePermission
from django.core.exceptions import  PermissionDenied

class HasCredentials(BasePermission):
 
    def has_permission(self, request, view):
    # Ne donnons l’accès qu’aux utilisateurs administrateurs authentifiés
        if request.user.is_authenticated:
            all_permissions_in_groups = request.user.get_group_permissions()
            if 'employees.view_employees'in all_permissions_in_groups:
                return True
            else: 
                raise PermissionDenied
            # return bool(request.user and request.user.is_authenticated and request.user.is_superuser)
        else: raise PermissionDenied