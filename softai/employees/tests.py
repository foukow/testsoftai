from django.test import TestCase
from django.urls import reverse_lazy
from rest_framework.test import APITestCase
from employees.models import Employees
class TestEmployees(APITestCase):
    url=reverse_lazy("employees")

    def employees_client(self):
        employe1=Employees.objects.create(name="Foukow", is_active=True)
        employe1.save();
        response=self.client.get(self.url)
        self.assertEqual(response.status_code,200)
        excepted=[{
            'id':employe1.pk,
            'name':employe1.name,
            'is_active':employe1.is_active
        }]
        self.assertEqual(excepted, response.json)


