# Generated by Django 4.1.3 on 2022-12-05 16:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='clients',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
