from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from clients.models import Clients
from serializers import ClientsSerializer
 
class ClientsAPIView(APIView):
 
    def get(self, *args, **kwargs):
        categories = Clients.objects.all()
        serializer = ClientsSerializer(categories, many=True)
        return Response(serializer.data)

class ClientsViewset(ModelViewSet):
 
    serializer_class = ClientsSerializer
 
    def get_queryset(self):
        return Clients.objects.all()